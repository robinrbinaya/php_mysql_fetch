<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="menu.php">B23 Store</a>
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="navbar-nav ml-auto">

    <?php 
    //if no user is logged in
    if(!isset($_SESSION['user_id'])) {?>
      <li class="nav-item">
        <a class="nav-link" href="menu.php">Catalogue</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">Login</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="register.php">Register</a>
      </li>
    <?php }
    //if a non-admin user is logged in
    else {
      if($_SESSION['isAdmin'] == 'false') { ?>
        <li class="nav-item">
          <a class="nav-link" href="menu.php">Catalogue</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cart.php">Cart
            <span id="cartCount"><?php 
            //if session cart is set and has a count,
            if(isset($_SESSION['cart']) && count($_SESSION['cart'])) {
              //print the sum of the session cart array
              echo array_sum($_SESSION['cart']);
            } 
            ?></span>
          </a>
        </li>
        <li class = "nav-item">
          <a class="nav-link" href="transactions.php">My Orders</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../controllers/logout.php">Logout</a>
        </li>
      <?php } 
      //if an admin is logged in
      else { ?>
        <li class="nav-item">
          <a class="nav-link" href="menu.php">Products Dashboard</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../controllers/logout.php">Logout</a>
        </li>
      <?php };
    }; ?>
  </ul>
</nav>