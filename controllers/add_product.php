<?php 
	require "connection.php";
	//sanitize form inputs
	$name = htmlspecialchars($_POST['name']);
	$price = htmlspecialchars($_POST['price']);
	$desc = htmlspecialchars($_POST['description']);
	$stocks = htmlspecialchars($_POST['stocks']);
	$category = htmlspecialchars($_POST['category']);

	//save the image properties as variables
	$filename = $_FILES['image']['name'];
	$filesize = $_FILES['image']['size'];
	$file_tmpname = $_FILES['image']['tmp_name'];
	//get the file extension property of $filename using pathinfo function, convert to lowercase chars
	$file_type = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
	//$_FILES[name in the form][property]
	//list of properties commonly used:
	//name, size, tmp_name, type
	$hasDetails = false;
	$isImg = false;
	if($name != "" && $price > 0 && $desc != ""){
		$hasDetails = true;
	}
	if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg"){
		$isImg = true;
	}
	if($filesize > 0 && $isImg && $hasDetails){
		$final_filepath = "../assets/images/" . $filename;
		//move image temporarily stored image to final file path
		move_uploaded_file($file_tmpname, $final_filepath);
	} else {
		echo "Please upload an image";
	}


	$sql = "INSERT INTO items (name, price, description, image, stocks, category_id) VALUES ('$name', '$price', '$desc', '$final_filepath', '$stocks', '$category')";

	mysqli_query($conn, $sql) or die(mysqli_error($conn));

	header('location: '.$_SERVER['HTTP_REFERER']');	