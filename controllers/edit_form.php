<?php

require "connection.php";

$sql = "SELECT * FROM category";
$categories = mysqli_query($conn, $sql) or die(mysqli_error($conn));

$id = $_GET['id'];
$sql = "SELECT * FROM items WHERE itemID = $id";
$item = mysqli_fetch_assoc(mysqli_query($conn, $sql));

?>

<form method="post" action="../controllers/edit_item.php?id=<?= $item['itemID']; ?>" enctype="multipart/form-data">
	<div class="form-group">
		<label for="name">Name: </label>
		<input class="form-control" type="text" id="name" name="name" value="<?= $item['name']; ?>">
	</div>
	<div class="form-group">
		<label for="description">Description : </label>
		<input class="form-control" type="text" id="description" name="description" value="<?= $item['description']; ?>">
	</div>
	<div class="form-group">
		<label for="price">Price: </label> 
		<input class="form-control" type="number" id="price" name="price" value="<?= $item['price']; ?>">
	</div>
	<div class="form-group">
		<label for="price">Stocks: </label> 
		<input class="form-control" type="number" id="stocks" name="stocks" value="<?= $item['stocks']; ?>">
	</div>
	<div class="form-group">
		<label for="category_id">Category: </label> 
		<select class="form-control" id="category_id" name="category_id">
			<?php foreach($categories as $category) : ?>
				<?php if($category['categoryID'] == $item['category_id'])  : ?>
					<option selected value="<?= $category['categoryID']; ?>"><?= $category['description']; ?></option>
				<?php else : ?>
					<option value="<?= $category['categoryID']; ?>"><?= $category['description']; ?></option>
				<?php endif; ?>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="form-group">
		<label for="image">Image: </label>
			<input class="form-control" type="file" id="image" name="image">
	</div>
	<button class="btn btn-success">Edit Item</button>
</form>