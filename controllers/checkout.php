<?php  

session_start();
require "connection.php";

if(isset($_SESSION['cart'])) {
	$user_id = $_SESSION['user_id'];
	$date = date('Y-m-d H:i:s');
	$total = 0;
	$sql = "INSERT INTO orders (userID) VALUES ($user_id)";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));
	$order_id = mysqli_insert_id($conn);
	foreach ($_SESSION['cart'] as $id => $quantity) {
		$sql = "SELECT * FROM items WHERE itemID = $id";
		$item = mysqli_fetch_assoc(mysqli_query($conn, $sql));
		$subtotal = $item['price'] * $quantity;
		$total += $subtotal;

		$sql = "INSERT INTO item_order (itemID, quantity, orderID, subtotal) VALUES ($id, $quantity, $order_id, $subtotal)";
		mysqli_query($conn, $sql) or die(mysqli_error($conn));
	}
	$refNo = mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9).mt_rand(0,9);
	$sql = "UPDATE orders SET total = '$total', timeNow = '$date', refNo = '$refNo' WHERE orderID = '$order_id';";
	mysqli_query($conn, $sql) or die(mysqli_error($conn));
	unset($_SESSION['cart']);

	header('location: ../views/transactions.php');
}

?>