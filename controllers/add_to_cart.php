<?php

session_start();

$id = $_POST['id'];
$quantity = $_POST['quantity'];

if(isset($_POST["fromCartPage"])){ //edits are from the cart page
	$_SESSION['cart'][$id] = $quantity;
	header("Location: ". $_SERVER["HTTP_REFERER"]);
} else {
	if(isset($_SESSION['cart'][$id])){
		$_SESSION['cart'][$id] += $quantity;
		//add the quantity if there is already a value for that item
	} else {
		$_SESSION['cart'][$id] = $quantity;
		//set the item's quantity
	}
}