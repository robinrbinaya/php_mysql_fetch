<?php

$title = "Transaction History";

function get_content() {
	require "../controllers/connection.php";
	$sql = "SELECT * FROM orders WHERE userID = ".$_SESSION['user_id'];
	$result = mysqli_query($conn, $sql); ?>
	<div id="accordion">
	<?php foreach($result as $order) : ?>
		<div class="card">
			<div class="card-header">
				<a class="card-link" data-toggle="collapse" href="#collapse<?= $order['orderID']; ?>">
					Reference #<?= $order['refNo']; ?>
				</a>
				<span class="float-right"><?= $order['timeNow']; ?></span>
			</div>
			<div id="collapse<?= $order['orderID']; ?>" class="collapse" data-parent="#accordion">
				<div class="card-body">
					<table class="table">
						<thead>
							<tr>
								<th>Item</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$sql = "SELECT * FROM item_order io JOIN items i ON (io.itemID = i.itemID) WHERE orderID = ".$order['orderID'];
							$details = mysqli_query($conn, $sql);
							foreach($details as $detail) : 
							//$subtotal = $detail['price'] * $detail['quantity'];
							?>
							<tr>
								<td><img src="<?= $detail['image']; ?>" width="25"><?= $detail['name']; ?></td>
								<td><?= $detail['price'] ?></td>
								<td><?= $detail['quantity'] ;?></td>
								<td><?= number_format($detail['subtotal'], 2); ?></td>
							</tr>
						<?php endforeach; ?>
						<tr>
							<td></td><td></td><td></td>
							<td>
								Total: Php <?= number_format($order['total'],2); ?>	
							</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php endforeach;
}

require "../partials/template.php";
?>