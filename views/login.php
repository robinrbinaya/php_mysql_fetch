<?php 

$title = "Login Form";

function get_content() { ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-8 offset-2">
				<h1> Login Form </h1>

				<form method="post" action="../controllers/login.php">
					<div class="form-group row">
						<label for="email" class="col-3 text-right">Email:</label>
						<input type="email" class="form-control col-9" id="email" name="email">
						<span style="color:red" id="emailError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="password" class="col-3 text-right">Password:</label>
						<input type="password" class="form-control col-9" id="password" name="password">
						<span style="color:red" id="passwordError" class="col-9 offset-3"></span>
					</div>
					<button type="submit" id="loginBtn" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>

<?php };

require_once "../partials/template.php";

?>
