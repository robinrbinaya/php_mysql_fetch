<?php $title = "Registration";

function get_content() { ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-8 offset-2">
				<h1> Registration Form </h1>
				<form method="POST" action="../controllers/register.php">
					
					<div class="form-group row">
						<label for="fName" class="col-3 text-right">First Name:</label>
						<input type="text" class="form-control col-9" id="fName" name="firstName">
						<span style="color:red" id="fNameError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="lName" class="col-3 text-right">Last Name:</label>
						<input type="text" class="form-control col-9" id="lName" name="lastName">
						<span style="color:red" id="lNameError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="email" class="col-3 text-right">Email:</label>
						<input type="email" class="form-control col-9" id="email" name="email">
						<span style="color:red" id="emailError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="pwd" class="col-3 text-right">Password:</label>
						<input type="password" class="form-control col-9" id="pwd" name="pwd">
						<span style="color:red" id="pwdError" class="col-9 offset-3"></span>
					</div>
					<div class="form-group row">
						<label for="confirmPwd" class="col-3 text-right">Confirm Password:</label>
						<input type="password" class="form-control col-9" id="confirmPwd">
						<span style="color:red" id="cpwdError" class="col-9 offset-3"></span>
					</div>
					<button type="submit" id="submitBtn" class="btn btn-primary" disabled>Submit</button>
					<!-- <span style="color:green" id="registered" class="col-9 offset-3"></span>
					<span style="color:red" id="regError" class="col-9 offset-3"></span> -->
				</form>
			</div>
		</div>
	</div>
<?php }

require_once "../partials/template.php";

?>

<script type="text/javascript">

	const fName = document.querySelector("#fName");
	const lName = document.querySelector("#lName");
	const email = document.querySelector("#email");
	const pwd = document.querySelector("#pwd");
	const confirmPwd = document.querySelector("#confirmPwd");
	const pwdError = document.querySelector("#pwdError");
	const regBtn = document.querySelector("#submitBtn");

	const inputs = document.querySelectorAll("input.form-control");

	function checkEmail() {
		return fetch('../controllers/check_email.php?email='+email.value)
		.then( function(res){
			return res.text();
		})
		.then( function(data){
			let flag = false;
			if(data == 'not available') {
				email.nextElementSibling.innerHTML = "email already exists";
				flag = true;
			} else {
				email.nextElementSibling.innerHTML = "";
			}

			return flag;
		});
	}

	inputs.forEach( function(input) {
		input.addEventListener("input", function() {
			if(this.value == "") {
				this.nextElementSibling.innerHTML = "this field is required";
			} else {
				this.nextElementSibling.innerHTML = "";
			}

			checker();
		});
	});

	function checker() {
		let errorFlag = false;
		inputs.forEach( function(inp) {
			if(inp.value == "") {
				errorFlag = true;
			}
		});

		let pwdVal = pwd.value;
		let cpwdVal = confirmPwd.value;
		if(pwdVal!="" && pwdVal != cpwdVal) {
			errorFlag = true;
		}

		checkEmail()
		.then( function(data) {
			if(!data && !errorFlag) {
				document.querySelector("#submitBtn").disabled = false;
			} else {
				document.querySelector("#submitBtn").disabled = true;
			}
		});
	}

	function confirmPassword() {
			let pwdVal = pwd.value;
			let cpwdVal = confirmPwd.value;
			if(pwdVal!="" && pwdVal != cpwdVal) {
				cpwdError.innerHTML = "passwords did not match";
			} else {
				cpwdError.innerHTML = "";
			}
	}

	confirmPwd.addEventListener("input", function() {
		let pwdVal = pwd.value;
		let cpwdVal = confirmPwd.value;
		if(pwdVal!="") confirmPassword();
	});

	pwd.addEventListener("input", function() {
		let pwdVal = pwd.value;
		let cpwdVal = confirmPwd.value;
		if(pwdVal!="") confirmPassword();
	});
	
	/*regBtn.addEventListener("click", function() {
		let data = new FormData;
		data.append('firstName', fName.value);
		data.append('lastName', lName.value);
		data.append('email', email.value);
		data.append('password', pwd.value);

		fetch('../controllers/register.php', {
			method: 'post',
			body: data
		})
		.then(function(res) {
			return res.text();
		})
		.then(function(data) {
			registered.innerHTML = data;
		})
		.catch(function(err) {
            console.log(err);
        });
	})*/
</script>