<?php 

session_start();
$title = "Menu Page";

function get_content() { 
	require "../controllers/connection.php";

	$sort = "";
	if(isset($_SESSION['sort'])) $sort = $_SESSION['sort'];

	$sql = "SELECT * FROM category";
	$categories = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	if(isset($_GET['cat'])) {
		$sql = "SELECT * FROM items WHERE category_id =".$_GET['cat'].$sort;
	} else {
		$sql = "SELECT * FROM items $sort";
	}
	$items_array = mysqli_query($conn, $sql) or die(mysqli_error($conn)); ?>

	<div class="modal" id="deleteModal">
	  <div class="modal-dialog">
	    <div class="modal-content">

	      <!-- Modal Header -->
	      <div class="modal-header">
	        <h4 class="modal-title" id="deleteModalTitle"></h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>

	      <!-- Modal body -->
	      <div class="modal-body" id="deleteModalBody">
	        Are you sure you want to delete
	      </div>

	      <!-- Modal footer -->
	      <div class="modal-footer">
	      	<a class="btn btn-danger" id="modalDeleteBtn">Delete</a>
	        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>

	<div class="modal" id="editModal">
	  <div class="modal-dialog">
	    <div class="modal-content">

	      <!-- Modal Header -->
	      <div class="modal-header">
	        <h4 class="modal-title" id="editModalTitle">Edit Item</h4>
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	      </div>

	      <!-- Modal body -->
	      <div class="modal-body" id="editModalBody">
	        
	      </div>

	      <!-- Modal footer -->
	      <div class="modal-footer">
	      	<a class="btn btn-danger" id="modalDeleteBtn">Delete</a>
	        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
	      </div>

	    </div>
	  </div>
	</div>

	<div id="mySidebar" class="sidebar">
	  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	  <h3 style="margin: 60px 0 30px 20px; color: white;">Filter By Category:</h3>
	  <?php foreach($categories as $category) : ?>
	  	<a href="menu.php?cat=<?= $category['categoryID']; ?>"><?= $category['description']; ?></a>
	  <?php endforeach; ?>
	  <a href="menu.php">All</a>

	  <h3 style="margin: 60px 0 30px 20px; color: white;">Sort By Price:</h3>
	  <a href="../controllers/sort.php?sort=asc">Lowest to Highest</a>
	  <a href="../controllers/sort.php?sort=desc">Highest to Lowest</a>
	</div>

	<div id="main">
	  <button class="openbtn" onclick="openNav()">&#9776; Sort & Filter</button>
	</div> 

	<div id="accordion">

		<div class="card">
		    <div class="card-header">
		        <a class="card-link h3" data-toggle="collapse" href="#addCategory">
		        Add Category
		        </a>
		    </div>
		    <div id="addCategory" class="collapse" data-parent="#accordion">
		        <div class="card-body">
		            <form>
		        	    <div class="form-group">
			        		<label for="name">Name: </label>
			        		<input class="form-control" type="text" id="catName" name="description">
			        	</div>
			        	<button class="btn btn-success" id="addCatBtn">Add Category</button>
			        </form>
		        </div>
		    </div>
		</div>

		<div class="card">
		    <div class="card-header">
		        <a class="card-link h3" data-toggle="collapse" href="#addItem">
		        Add Item
		        </a>
		    </div>
		    <div id="addItem" class="collapse" data-parent="#accordion">
		        <div class="card-body">
		            <form method="post" action="../controllers/add_product.php" enctype="multipart/form-data">
		        	    <div class="form-group">
			        		<label for="name">Name: </label>
			        		<input class="form-control" type="text" id="name" name="name">
			        	</div>
			        	<div class="form-group">
			        		<label for="description">Description : </label>
			        		<input class="form-control" type="text" id="description" name="description">
			        	</div>
			        	<div class="form-group">
			        		<label for="price">Price: </label> 
			        		<input class="form-control" type="number" id="price" name="price">
			        	</div>
			        	<div class="form-group">
			        		<label for="stocks">Stocks: </label> 
			        		<input class="form-control" type="number" id="stocks" name="stocks">
			        	</div>
			        	<div class="form-group">
			        		<label for="category_id">Category: </label> 
			        		<select class="form-control" id="category_id" name="category">
			        			<?php foreach($categories as $category) : ?>
			        			<option value="<?= $category['categoryID']; ?>"><?= $category['description']; ?></option>
			        			<?php endforeach; ?>
			        		</select>
			        	</div>
			        	<div class="form-group">
			        		<label for="image">Image: </label>
			        			<input class="form-control" type="file" id="image" name="image">
			        	</div>
			        	<button class="btn btn-success">Add Item</button>
			        </form>
		        </div>
		    </div>
		</div>
	<div class="row">
<?php
	foreach($items_array as $item) : ?>
	<div class="col-3 p-4">
		<div class="card p-4">
			<div style="width:50%;" class="mx-auto">
		  <img class="card-img-top" width="100%" src="<?php echo $item['image']; ?>"></div>
		  <div class="card-body" data-id="<?php echo $item['itemID']; ?>" data-name="<?php echo $item['name']; ?>">
		    <h4 class="card-title"><?php echo $item['name']; ?></h4>
		    <p class="card-text"><?php echo $item['description']; ?></p>
		    <p class="card-text"><?php echo $item['price']; ?></p>
			<?php if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 'false') : ?>
				<input class="quantity" type="number" min="1" name="quantity">
				<button data-id="<?= $item['itemID']; ?>" class="add-to-cart">Add to cart!</button>
			<?php elseif(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 'true') : ?>
				<button class="btn btn-primary editBtn" data-toggle="modal" data-target="#editModal">Edit</button>
				<button class="btn btn-danger deleteBtn" data-toggle="modal" data-target="#deleteModal">Delete</button>
			<?php endif; ?>
			</div>
		</div>
	</div>
<?php endforeach; ?>
</div>
<link rel="stylesheet" type="text/css" href="../assets/css/sidepanel.css">
<script type="text/javascript">
	addCatBtn.addEventListener("click", function() {
		let data = new FormData;
		data.append('category', catName.value);

		fetch("../controllers/add_category.php", {
			method: "post",
			body: data
		})
		.then(function(res){
			return res.text();
		})
		.then(function(data){
			category_id.innerHTML += data;
		})
		.catch(function(error) {
			console.log('Oops something went wrong! Error code: ', error);
		}) 
	})

	const addToCartBtns = document.querySelectorAll('.add-to-cart');
	addToCartBtns.forEach( function(button) {
		button.addEventListener('click', function(){
			const quantityField = this.parentElement.querySelector('.quantity');
			const quantity = quantityField.value;
			const id = button.getAttribute('data-id');
			quantityField.value = "";

			let formBody = new FormData;
			formBody.append('id', id);
			formBody.append('quantity', quantity);

			fetch('../controllers/add_to_cart.php', {
				method : 'post',
				body : formBody
			})
			.then( function(res) {
				return res.text();
			})
			.then( function(data) {
				console.log(cartCount.innerHTML)
				if(cartCount.innerHTML != '')
					cartCount.innerHTML = parseInt(cartCount.innerHTML) + parseInt(quantity);
				else
					cartCount.innerHTML =parseInt(quantity);
				alert('item added to cart');
			});
		});
	});

	document.querySelectorAll('.editBtn')
	.forEach( function(btn){
		btn.addEventListener('click', function(){
			let id = btn.parentElement.getAttribute('data-id');
			fetch('../controllers/edit_form.php?id='+id)
			.then( function(res){
				return res.text();
			})
			.then( function(data){
				editModalBody.innerHTML = data;
			});
		});
	});

	document.querySelectorAll('.deleteBtn')
	.forEach( function(btn){
		btn.addEventListener('click', function() {
			let id = btn.parentElement.getAttribute('data-id');
			let name = btn.parentElement.getAttribute('data-name');
			deleteModalTitle.innerHTML = name;
			deleteModalBody.innerHTML += name+'?';
			modalDeleteBtn.href = '../controllers/delete_item.php?id='+id;
		});
	});

	function openNav() {
	  document.getElementById("mySidebar").style.width = "20%";
	  document.getElementById("main").style.marginLeft = "20%";
	}

	function closeNav() {
	  document.getElementById("mySidebar").style.width = "0";
	  document.getElementById("main").style.marginLeft = "0";
	}
</script>
<?php }
	require "../partials/template.php";
?>