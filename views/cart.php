<?php

session_start();
$title = "Cart";

function get_content() {
	require "../controllers/connection.php";
	if(isset($_SESSION['cart']) && count($_SESSION['cart'])) : ?>
		<table class="table">
			<thead>
				<tr>
					<th>Item</th>
					<th>Price</th>
					<th>Quantity</th>
					<th>Subtotal</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
		<?php $total = 0;

		foreach($_SESSION['cart'] as $id => $quantity) :
			$sql = "SELECT * FROM items WHERE itemID = $id";
			$item = mysqli_fetch_assoc(mysqli_query($conn, $sql));

			extract($item);
			$subtotal = $price * $quantity;
			$total += $subtotal; ?>
			<tr id="row<?= $id ?>">
				<td><img src="<?= $image ?>" width="25"><?= $name ?></td>
				<td><?= $price ?></td>
				<td><form method="post" action="../controllers/add_to_cart.php" id="updateCart">
					<input type="hidden" name="id" value="<?= $id ?>">
					<input type="hidden" name="updateCart" value="true">
					<input type="number" name="quantity" value="<?= $quantity ?>" class="quantityInput"></td>
				<td><?= number_format($subtotal, 2); ?></td>
				<td>
					<form method="POST" action="../controllers/remove_from_cart.php">
						<input type="hidden" name="id" value="<?= $id; ?>">
						<button class="btn btn-danger">Remove From Cart</button>
					</form>
				</td>
			</tr>	
		<?php endforeach; ?>
	<tr>
		<td></td><td></td><td></td>
		<td>
			Total: Php <?= number_format($total,2); ?>	
		</td>
		<td>
			<button class="btn btn-danger">Empty Cart</button>
			<a href="../controllers/checkout.php" class="btn btn-primary">Proceed to Checkout</a>
			<!-- <span id="paypal-button-container"></span> -->
		</td>
	</tr>
	</tbody>
</table>
<!-- <script src="https://www.paypal.com/sdk/js?client-id=Ad7r1ZxK3ri-oFRhmIsSIGAgNkWyAIhuGQTCvuP3agsnSbVMMqzhVKhRKfAo-QE9uSyxaMnxh3bebLr5&currency=PHP"></script> -->

<script>
	let qInputs = document.querySelectorAll('.quantityInput');
	qInputs.forEach( function(qInput){
		qInput.addEventListener('blur', function(){
			qInput.parentElement.submit();
		});
	});
	paypal.Buttons({
	    createOrder: function(data, actions) {
	      // Set up the transaction
	      return actions.order.create({
	        purchase_units: [{
	          amount: {
	            value: '<?= number_format($total,2); ?>'
	          }
	        }]
	      });
	    },
	    onApprove: function(data, actions) {
          return actions.order.capture().then(function(details) {
            alert('Transaction completed by ' + details.payer.name.given_name);
            // Call your server to save the transaction
            // return fetch('/paypal-transaction-complete', {
            //   method: 'post',
            //   body: JSON.stringify({
            //     orderID: data.orderID
            //   })
            // });
            console.log(data);
          });
        }
	  }).render('#paypal-button-container');
</script>
	<?php else :
		echo "<h2>No items in cart.</h2>";
	endif;
}

require "../partials/template.php";

?>